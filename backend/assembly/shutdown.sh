#!/bin/bash

BASEDIR=$(dirname $0)

if [ -f $BASEDIR/app.pid ]
then
  PID=`cat $BASEDIR/app.pid`
  if ps -p $PID > /dev/null
  then
    # process was found
    echo "Stopping application"
  else
    # process not found
    echo "Process not running"
    rm -f $BASEDIR/app.pid
    exit
  fi
else
  echo "Cannot find app.pid"
  exit
fi

kill $PID
rm -f $BASEDIR/app.pid
echo "Stopped process with ID $PID"