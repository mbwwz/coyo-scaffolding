(function () {
  'use strict';
  var webdriver = require('selenium-webdriver');
  var Navigation = require('../../navigation.page.js');
  var login = require('../../login.page.js');
  var testhelper = require('../../testhelper.js');
  var SelectLibrary = require('./file-library.home.js');

  describe('file-library', function () {
    var PAGE_SIZE = 30;
    var navigation, selectLibrary;

    beforeEach(function () {
      navigation = new Navigation();
      selectLibrary = new SelectLibrary();

      login.loginDefaultUser();
    });

    afterEach(function () {
      testhelper.deletePages();
    });

    /* it(description, function, timeOut)
     * timeout is set to 3 Minutes (180000ms) for this test because of the time needed to return promises.
     */
    it('check endless scrolling', function () {
      // create pages
      var promises = [];
      for (var i = 0; i < PAGE_SIZE + 1; i++) {
        promises.push(testhelper.createPage('EndlessScrollingTestPage_' + i));
      }

      webdriver.promise.all(promises).then(function () {
        // navigate to fileLibrary
        navigation.profileMenu.open();
        navigation.profileMenu.fileLibrary.click();

        selectLibrary.library1.libraryHome.click();
        selectLibrary.documentLibrary.pages.click();

        expect(element.all(by.css('.fl-table-row')).count()).toBe(30);
        // do scrolling
        browser.executeScript('$(".modal").scrollTop($(".modal")[0].scrollHeight)').then(function () {
          expect(element.all(by.css('.fl-table-row')).count()).toBeGreaterThan(30);
        });

        selectLibrary.closeModalButton.click();
      });
    }, 180000);
  });

})();
