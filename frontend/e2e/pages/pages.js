(function () {
  'use strict';

  var Navigation = require('../navigation.page.js');
  var PageList = require('./page-list.page.js');
  var PageCreate = require('./page-create.page.js');
  var PageDetails = require('./page-details.page.js');
  var login = require('../login.page.js');
  var testhelper = require('../testhelper.js');

  describe('pages', function () {
    var pageList, pageCreate, pageDetails, navigation, suffix;

    beforeEach(function () {
      pageList = new PageList();
      pageCreate = new PageCreate();
      pageDetails = new PageDetails();
      navigation = new Navigation();
      suffix = Math.floor(Math.random() * 1000000);

      login.loginDefaultUser();
      navigation.pages.click();
      testhelper.cancelTour();
    });

    it('create a new page', function () {
      pageList.newButton.click();

      // invalid name
      pageCreate.page1.name.sendKeys('Company News');
      pageCreate.page1.name.sendKeys(protractor.Key.TAB);
      expect(pageCreate.page1.nameTaken.isPresent()).toBe(true);

      // valid name
      pageCreate.page1.name.clear();
      pageCreate.page1.name.sendKeys('Newpage' + suffix);
      pageCreate.page1.name.sendKeys(protractor.Key.TAB);
      expect(pageCreate.page1.nameTaken.isPresent()).toBe(false);

      pageCreate.page1.category.openDropdown();
      pageCreate.page1.category.selectOption('Company');

      pageCreate.page1.continueButton.click();

      // TODO user selection

      pageCreate.page2.continueButton.click();
      pageCreate.page3.createButton.click();
      expect(pageDetails.title.isPresent()).toBe(true);
    });
  });

})();
