(function () {
  'use strict';

  var testhelper = require('./testhelper');

  var loginPage = {
    get: function () {
      browser.get('/f/login');
      this.logout(); // prevent redirect to main state if already logged in
      testhelper.disableAnimations();
    },
    loginDefaultUser: function (doNotHideTour) {
      this.login('ian.bold@coyo4.com', 'demo', doNotHideTour);
    },
    loginDefaultAdminUser: function (doNotHideTour) {
      this.loginDefaultUser(doNotHideTour);
    },
    login: function (username, password, doNotHideTour) {
      this.get();
      loginPage.selectDefaultBackend();
      this.usernameInput.isPresent().then(function (isPresent) {
        if (isPresent) {
          loginPage.usernameInput.clear(); // clear field before enter new username
          loginPage.usernameInput.sendKeys(username);
          loginPage.passwordInput.sendKeys(password);
          loginPage.loginButton.click();
          // browser.sleep(1000); // TODO why do we need this? should be handled by protractor...
          if (!doNotHideTour) {
            testhelper.cancelTour();
          }
        }
      });
    },
    logout: function () {
      browser.executeAsyncScript(function (callback) {
        var injector = $('body').injector();
        injector.get('authService').logout().then(callback);
      });
    },
    selectDefaultBackend: function () {
      loginPage.backendSelector.urlInput.isPresent().then(function (isPresent) {
        if (isPresent) {
          loginPage.backendSelector.urlInput.sendKeys('http://localhost:8080');
          loginPage.backendSelector.continueButton.click();
        }
      });
    },
    clearUsernameInputField: function () {
      this.usernameInput.isPresent().then(function (isPresent) {
        if (isPresent) {
          loginPage.usernameInput.clear();
        }
      });
    },
    backendSelector: {
      urlInput: element(by.model('$ctrl.url')),
      continueButton: $('span[translate="MODULE.LOGIN.CONFIGURE.SUBMIT"]')
    },
    usernameInput: element(by.model('$ctrl.user.username')),
    passwordInput: element(by.model('$ctrl.user.password')),
    loginButton: $('button[type="submit"]'),
    logoutButton: $('a[ng-click="$ctrl.logout()"]')
  };

  module.exports = loginPage;

})();
