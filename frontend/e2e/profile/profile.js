(function () {
  'use strict';

  var NavigationPage = require('../navigation.page.js');
  var ProfilePage = require('./profile.page.js');
  var login = require('../login.page.js');
  var testhelper = require('../testhelper.js');

  describe('profile', function () {
    var navigation, profile, phone, mobile, xing, linkedin, twitter, facebook;

    beforeEach(function () {
      navigation = new NavigationPage();
      profile = new ProfilePage();
      phone = '+49 (0) 40 8859';
      mobile = '+160 1234 5678';
      xing = 'http://www.xing.com/abc';
      linkedin = 'http://www.linkedin.com/abc';
      twitter = 'http://www.twitter.com/abc';
      facebook = 'http://www.facebook.com/abc';

      login.loginDefaultUser();
    });

    it('user should see profile information', function () {
      navigation.profile.click();
      testhelper.cancelTour();

      profile.info.click();
      profile.contact.editButton.click();
      profile.contact.phoneInput.clear();
      profile.contact.phoneInput.sendKeys(phone);
      profile.contact.mobileInput.clear();
      profile.contact.mobileInput.sendKeys(mobile);
      profile.contact.xingInput.clear();
      profile.contact.xingInput.sendKeys(xing);
      profile.contact.linkedinInput.clear();
      profile.contact.linkedinInput.sendKeys(linkedin);
      profile.contact.twitterInput.clear();
      profile.contact.twitterInput.sendKeys(twitter);
      profile.contact.facebookInput.clear();
      profile.contact.facebookInput.sendKeys(facebook);

      profile.contact.submitButton.click();

      expect(profile.contact.phone.getText()).toBe(phone);
      expect(profile.contact.mobile.getText()).toBe(mobile);
      expect(profile.contact.xing.getText()).toBe(xing);
      expect(profile.contact.linkedin.getText()).toBe(linkedin);
      expect(profile.contact.twitter.getText()).toBe(twitter);
      expect(profile.contact.facebook.getText()).toBe(facebook);
    });
  });

})();
