(function () {
  'use strict';

  var login = require('../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var BlogArticleWidget = require('./blog-article-widget.page.js');
  var components = require('../../components.page');
  var testhelper = require('../../testhelper');

  describe('blog article widget', function () {
    var widgetSlot, widgetChooser, widget, blogArticleWidget, navigation;

    beforeEach(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      navigation = new Navigation();
      navigation.editView();

      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
      blogArticleWidget = new BlogArticleWidget(widget);
    });

    it('should be created', function () {
      expect(widgetSlot.allWidgets.count()).toBe(0);

      // new widget
      widgetSlot.addButton.click();
      widgetChooser.selectByName('Blog Article');

      blogArticleWidget.settings.blogArticle.openDropdown();
      blogArticleWidget.settings.blogArticle.selectOption('Mindsmash ist Top-Innovator 2014');

      // save
      widgetChooser.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(1);
      expect(blogArticleWidget.renderedWidget.articleSender.getText()).toBe('About Coyo');
      expect(blogArticleWidget.renderedWidget.articleTitle.getText()).toBe('Mindsmash ist Top-Innovator 2014');
      expect(blogArticleWidget.renderedWidget.teaserText.getText()).toMatch(/^Die Hamburger mindsmash GmbH punktet beim "Top 100"-Award/);

      // remove widget
      widget.hover();
      widget.removeButton.click();
      components.modals.confirm.confirmButton.click();
      navigation.viewEditOptions.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(0);

      expect(widgetSlot.addButton.isPresent()).toBe(false);
    });
  });

})();
