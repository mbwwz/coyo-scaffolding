(function () {
  'use strict';

  var login = require('../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var BlogWidget = require('./blog-widget.page');
  var components = require('../../components.page');
  var testhelper = require('../../testhelper');

  describe('blog widget', function () {
    var widgetSlot, navigation;

    beforeEach(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('page-sidebar-bottom');
      navigation = new Navigation();
      navigation.editView();
    });

    it('should be created', function () {
      expect(widgetSlot.allWidgets.count()).toBe(0);

      // new widget
      widgetSlot.addButton.click();
      widgetSlot.widgetChooser.selectByName('Latest Blog Articles');

      // settings
      var settings = new BlogWidget().settings;
      settings.articleCount.clear();
      settings.articleCount.sendKeys('3');
      // TODO make this work
      // settings.blogApp.openDropdown();
      // settings.blogApp.selectOption('Blog 1');

      // save
      widgetSlot.widgetChooser.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(1);

      // TODO verify content, open link, ...

      // remove widget
      var blogWidget = new BlogWidget(widgetSlot.getWidget(0));
      blogWidget.hover();
      blogWidget.removeButton.click();
      components.modals.confirm.confirmButton.click();
      navigation.viewEditOptions.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(0);

      expect(widgetSlot.addButton.isPresent()).toBe(false);
    });
  });

})();
