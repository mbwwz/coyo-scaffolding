(function () {
  'use strict';

  var login = require('../../login.page');
  var Navigation = require('../../navigation.page');
  var WidgetSlot = require('../widget-slot.page');
  var RteWidget = require('./rte-widget.page.js');
  var components = require('../../components.page');
  var testhelper = require('../../testhelper');

  describe('rte widget', function () {
    var widgetSlot, widgetChooser, widget, rteWidget, navigation;

    beforeEach(function () {
      login.loginDefaultUser();
      testhelper.createAndOpenPage('testpage' + Math.floor(Math.random() * 1000000));
      widgetSlot = new WidgetSlot('content');
      navigation = new Navigation();
      navigation.editView();

      widgetChooser = widgetSlot.widgetChooser;
      widget = widgetSlot.getWidget(0);
      rteWidget = new RteWidget(widget);
    });

    it('should be created', function () {
      expect(widgetSlot.allWidgets.count()).toBe(0);

      // new widget
      widgetSlot.addButton.click();
      widgetChooser.selectByName('Rich Text Editor');

      // count
      expect(widgetSlot.allWidgets.count()).toBe(1);

      // remove widget
      rteWidget.hover();
      rteWidget.removeButton.click();
      components.modals.confirm.confirmButton.click();
      navigation.viewEditOptions.saveButton.click();
      expect(widgetSlot.allWidgets.count()).toBe(0);

      expect(widgetSlot.addButton.isPresent()).toBe(false);
    });
  });

})();
