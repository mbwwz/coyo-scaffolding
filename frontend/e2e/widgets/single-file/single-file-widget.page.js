(function () {
  'use strict';

  var extend = require('util')._extend;

  function SingleFileWidget(widget) {
    extend(this, widget);
  }

  module.exports = SingleFileWidget;
})();
