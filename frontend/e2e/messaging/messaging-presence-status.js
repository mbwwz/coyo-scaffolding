(function () {
  'use strict';

  var login = require('./../login.page.js');
  var sidebar = require('./messaging-sidebar.page.js');
  var NavigationPage = require('../navigation.page.js');
  var testhelper = require('../testhelper.js');

  var username, password;

  describe('presence status', function () {
    beforeAll(function () {
      login.loginDefaultUser();
      var key = Math.floor(Math.random() * 1000000);
      username = 'max.mustermann.' + key + '@mindsmash.com';
      password = 'Secret123';
      testhelper.createUser('Max', 'Mustermann' + key, username, password, ['User']);
      new NavigationPage().logout();
    });

    beforeEach(function () {
      login.login(username, password);
    });

    it('should see presence status as online', function () {
      expect(sidebar.onlineStatus.isPresent()).toBe(true);
    });

    it('should change presence status', function () {
      // toggle form
      sidebar.onlineStatus.click();
      expect(sidebar.presenceStatusView.parent.isPresent()).toBe(true);

      // click AWAY status
      sidebar.presenceStatusView.awayState.click();

      browser.sleep(50); // wait a little bit for web-socket

      // close form
      sidebar.presenceStatusView.close.click();

      // then
      expect(sidebar.onlineStatus.isPresent()).toBe(false);
      expect(sidebar.awayStatus.isPresent()).toBe(true);
    });
  });

})();
