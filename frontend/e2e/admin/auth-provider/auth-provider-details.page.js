(function () {
  'use strict';

  var Select = require('../../select.page.js');

  // TODO extract
  function hasClass(el, className) {
    return el.getAttribute('class').then(function (value) {
      return value.split(' ').indexOf(className) >= 0;
    });
  }

  module.exports = {
    getById: function (id) {
      browser.get('/admin/authentication-providers/edit/' + id);
    },
    name: element(by.model('$ctrl.authenticationProvider.name')),
    type: new Select(element(by.model('$ctrl.authenticationProvider.type')), false),
    active: element(by.model('$ctrl.authenticationProvider.active')),
    isActive: function () {
      return hasClass(this.active, 'checked');
    },
    saveButton: element(by.cssContainingText('.btn.btn-primary', 'Save')),
    isSaveButtonDisabled: function () {
      return this.saveButton.getAttribute('disabled').then(function (disabled) {
        return disabled === 'true';
      });
    },
    cancelButton: element(by.cssContainingText('.btn.btn-default', 'Cancel'))
  };

})();
