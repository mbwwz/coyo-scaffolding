(function () {
  'use strict';

  var Select = require('../../select.page.js');

  function hasClass(el, className) {
    return el.getAttribute('class').then(function (value) {
      return value.split(' ').indexOf(className) >= 0;
    });
  }

  module.exports = {
    getById: function (id) {
      browser.get('/admin/user-management/roles/edit/' + id);
    },
    name: element(by.model('vm.role.displayName')),
    groups: new Select(element(by.model('vm.groups')), true),
    defaultRole: element(by.model('vm.role.defaultRole')),
    isDefaultRole: function () {
      return hasClass(this.defaultRole, 'checked');
    },
    permission: function (key) {
      return $('#p_' + key);
    },
    isPermissionSet: function (key) {
      return hasClass(this.permission(key), 'checked');
    },
    saveButton: element(by.cssContainingText('.btn.btn-primary', 'Save')),
    isSaveButtonDisabled: function () {
      return this.saveButton.getAttribute('disabled').then(function (disabled) {
        return disabled === 'true';
      });
    },
    cancelButton: element(by.cssContainingText('.btn.btn-default', 'Cancel'))
  };

})();
