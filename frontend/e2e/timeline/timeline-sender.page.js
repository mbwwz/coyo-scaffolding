(function () {
  'use strict';

  function SenderTimeline() {
    var api = this;

    api.loadMore = $('button[ng-click="$ctrl.loadMore()"]');
    api.timelineItems = element.all(by.css('.animate-move'));

    var commentForm = $('.comments-form-inner');

    api.timelineItem = {
      likeBtn: $('.timeline-item-footer').$('.btn-likes-handle'),
      likeBtnText: $('.btn-likes-text'),
      comment: element(by.model('$ctrl.formModel.message')),
      emoji: commentForm.$('.emoji-picker-toggle'),
      attachment: commentForm.$('a[ngf-select="$ctrl.addAttachments($files, $invalidFiles)"]'),
      submitBtn: commentForm.$('button[label="SEND"]'),
      commentMessage: function (indexTimelineItem, indexComment) {
        return api.timelineItems.get(indexTimelineItem).$$('.comment-body').get(indexComment).$('.comment-message');
      }
    };

    api.addComment = function (message, index) {
      api.timelineItems.get(index).$('textarea[ng-model="$ctrl.formModel.message"]').sendKeys(message);
    };
  }

  module.exports = SenderTimeline;

})();
